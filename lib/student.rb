class Student
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def first_name
    @first_name
  end

  def last_name
    @last_name
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def courses
    @courses
  end

  def enroll(new_course)
    @courses.each do |course| 
      if course.conflicts_with?(new_course)
        raise "this course conflicts with another"
      end
    end
    @courses << new_course unless @courses.include?(new_course)
    new_course.students << self
  end

  def course_load
    result = Hash.new(0)
    @courses.each do |course|
      result[course.department] += course.credits
    end
    result
  end

end
